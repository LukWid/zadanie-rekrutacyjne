using UnityEngine;

namespace Utils
{
    public struct MovementBounds
    {
        #region Public Properties

        [field: SerializeField]
        public Vector3 LeftBottom { get; set; }
        [field: SerializeField]
        public Vector3 RightTop { get; set; }

        #endregion

        #region Public Methods

        public Vector2 ClampToBounds(Vector2 position)
        {
            Vector2 clampedPosition = position;
            clampedPosition.x = Mathf.Clamp(position.x, LeftBottom.x, RightTop.x);
            clampedPosition.y = Mathf.Clamp(position.y, LeftBottom.y, RightTop.y);

            return clampedPosition;
        }

        #endregion
    }

    public static class CameraExtensions
    {
        #region Public Methods

        public static MovementBounds GetMovementBounds(this Camera self)
        {
            MovementBounds bounds = new MovementBounds();
            bounds.LeftBottom = self.ScreenToWorldPoint(new Vector3(0, 0, self.nearClipPlane));
            bounds.RightTop = self.ScreenToWorldPoint(new Vector3(self.pixelWidth, self.pixelHeight, self.nearClipPlane));

            return bounds;
        }

        #endregion
    }
}