namespace Utils
{
    public static class SceneNames
    {
        #region Constants

        public const string MENU_SCENE = "MainMenu";
        public const string GAMEPLAY_SCENE = "Gameplay";

        #endregion
    }
}