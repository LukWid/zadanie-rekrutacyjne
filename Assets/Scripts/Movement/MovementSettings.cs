using UnityEngine;

namespace Movement
{
    [CreateAssetMenu(fileName = "MovementSettings", menuName = "Movement/MovementSettings")]
    public class MovementSettings : ScriptableObject
    {
        #region Public Properties

        [field: SerializeField]
        public float MovementSpeed { get; set; }

        #endregion
    }
}