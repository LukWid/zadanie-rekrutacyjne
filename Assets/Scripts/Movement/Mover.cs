using UnityEngine;
using Utils;

namespace Movement
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Mover : MonoBehaviour
    {
        #region Private Fields

        private Rigidbody2D rigidbody2D;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
        }

        #endregion

        #region Public Methods

        public void Move(Vector2 moveDirection, float movementSpeed)
        {
            if (rigidbody2D == null)
            {
                return;
            }

            rigidbody2D.velocity = moveDirection * movementSpeed;
            MovementBounds bounds = Camera.main.GetMovementBounds();
            transform.position = bounds.ClampToBounds(transform.position);
        }

        #endregion
    }
}