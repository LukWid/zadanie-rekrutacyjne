using UnityEngine;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField]
        private MainMenuPanel mainMenu;
        [SerializeField]
        private SettingsPanel settings;
    
        void Start()
        {
            Initialize();
            OpenMenu();
        }

        private void Initialize()
        {
            mainMenu.Initialize(this);
            settings.Initialize(this);
        }

        public void OpenMenu()
        {
            mainMenu.OpenPanel();
            settings.ClosePanel();
        }

        public void OpenSettings()
        {
            mainMenu.ClosePanel();
            settings.OpenPanel();
        }
    }
}
