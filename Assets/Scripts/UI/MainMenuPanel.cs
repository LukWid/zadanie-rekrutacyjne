using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace UI
{
    public class MainMenuPanel : ABasePanel
    {
        [SerializeField]
        private Button playButton;
        [SerializeField]
        private Button settingsButton;
        [SerializeField]
        private Button exitButton;

        void Start()
        {
            AssignCallbacks();
        }

        private void OnDestroy()
        {
            UnassignCallbacks();
        }

        private void AssignCallbacks()
        {
            playButton.onClick.AddListener(PlayGame);
            settingsButton.onClick.AddListener(OpenSettings);
            exitButton.onClick.AddListener(ExitGame);
        }

        private void UnassignCallbacks()
        {
            playButton.onClick.RemoveListener(PlayGame);
            settingsButton.onClick.RemoveListener(OpenSettings);
            exitButton.onClick.RemoveListener(ExitGame);
        }

        private void ExitGame()
        {
            Application.Quit();
        }

        private void OpenSettings()
        {
            Manager.OpenSettings();
        }

        private void PlayGame()
        {
            SceneManager.LoadScene(SceneNames.GAMEPLAY_SCENE);
        }
    }
}
