using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace UI
{
    public class SettingsPanel : ABasePanel
    {
    
        [SerializeField]
        private Button backButton;
        [SerializeField]
        private InputActionReference backKey;
        
        
        private void Start()
        {
            AssignCallbacks();
        }
    
        private void OnDestroy()
        {
            UnassignCallbacks();
        }

        private void AssignCallbacks()
        {
            backButton.onClick.AddListener(BackToMenu);
        }

        public override void OpenPanel()
        {
            base.OpenPanel();
            backKey.action.performed += BackFromKey;
        }

        private void BackFromKey(InputAction.CallbackContext obj)
        {
            BackToMenu();
        }

        public override void ClosePanel()
        {
            backKey.action.performed -= BackFromKey;
            base.ClosePanel();
        }

        private void UnassignCallbacks()
        {
            backButton.onClick.RemoveListener(BackToMenu);
        }
        private void BackToMenu()
        {
            Manager.OpenMenu();
        }
    }
}
