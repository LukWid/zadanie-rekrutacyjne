using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public abstract class ABasePanel : MonoBehaviour
    {

        [SerializeField]
        private GameObject firstSelectedGameObject;

        public UIManager Manager { get; private set; }

        public virtual void Initialize(UIManager uiManager)
        {
            this.Manager = uiManager;
        }

        public virtual void OpenPanel()
        {
            gameObject.SetActive(true);
            EventSystem.current.SetSelectedGameObject(firstSelectedGameObject);
        }

        public virtual void ClosePanel()
        {
            gameObject.SetActive(false);
        
        }
    }
}
