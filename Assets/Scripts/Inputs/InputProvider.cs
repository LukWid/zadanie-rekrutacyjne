using Inputs.Interfaces;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Inputs
{
    public class InputProvider : MonoBehaviour, IDirectionProvider
    {
        #region Private Fields

        private InputActions input;

        private Vector2 movementDirection;
        private bool mousePressed;
        private Player.Player player;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            input = new InputActions();
            input.Player.MouseButton.performed += _ => mousePressed = true;
            input.Player.MouseButton.canceled += _ => mousePressed = false;
            input.Enable();
        }

        #endregion

        #region Public Methods

        public Vector2 GetDirection()
        {
            if (mousePressed)
            {
                Vector3 mousePosition = Camera.main.ScreenToWorldPoint(input.Player.MousePosition.ReadValue<Vector2>());
                movementDirection = mousePosition - player.transform.position;

                return movementDirection.normalized;
            }

            movementDirection = input.Player.Move.ReadValue<Vector2>();

            return movementDirection;
        }

        public void Initialize(Player.Player player)
        {
            this.player = player;
        }

        #endregion
    }
}