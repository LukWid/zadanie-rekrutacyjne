using UnityEngine;

namespace Inputs.Interfaces
{
    public interface IDirectionProvider
    {
        #region Public Methods

        public Vector2 GetDirection();

        #endregion

        void Initialize(Player.Player player);
    }
}