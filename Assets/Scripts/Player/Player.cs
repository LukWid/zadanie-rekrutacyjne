using Enemies;
using Inputs.Interfaces;
using Movement;
using UnityEngine;

namespace Player
{
    public class Player : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private MovementSettings movementSettings;
        [SerializeField]
        private LayerMask enemyLayer;

        #endregion

        #region Private Fields

        private IDirectionProvider directionProvider;
        private Mover mover;
        private Collider2D enemyFound;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            CollectReferences();
            directionProvider.Initialize(this);
        }

        private void CollectReferences()
        {
            directionProvider = GetComponent<IDirectionProvider>();
            mover = GetComponent<Mover>();
        }

        private void FixedUpdate()
        {
            enemyFound = Physics2D.OverlapCircle(transform.position, 0.5f, enemyLayer);

            if (enemyFound == null)
            {
                return;
            }

            if (enemyFound.TryGetComponent(out Enemy enemy))
            {
                enemy.Kill();
            }
        }

        private void Update()
        {
            Vector2 moveDirection = directionProvider.GetDirection();
            mover.Move(moveDirection, movementSettings.MovementSpeed);
        }

        #endregion
    }
}