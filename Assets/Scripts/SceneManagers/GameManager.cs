using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Utils;

namespace SceneManagers
{
    public class GameManager : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private Player.Player playerPrefab;
        [SerializeField]
        private EnemySpawner enemySpawner;
        [SerializeField]
        private Camera gameCamera;
        [SerializeField]
        private InputActionReference backToMenuKey;

        #endregion

        
        #region Unity Callbacks

        private void Start()
        {
            backToMenuKey.action.performed += BackToMenu;
            MovementBounds movementBounds = gameCamera.GetMovementBounds();
            enemySpawner.SpawnEnemies(movementBounds, SpawnPlayer());
        }

        private void OnDestroy()
        {
            backToMenuKey.action.performed -= BackToMenu;
        }

        private void BackToMenu(InputAction.CallbackContext obj)
        {
            SceneManager.LoadScene(SceneNames.MENU_SCENE);
        }

        private Player.Player SpawnPlayer()
        {
            return Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        }

        #endregion
    }
}