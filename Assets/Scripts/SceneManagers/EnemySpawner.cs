using Enemies;
using UnityEngine;
using Utils;

namespace SceneManagers
{
    public class EnemySpawner : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private int numberOfEnemies;
        [SerializeField]
        private Enemy enemyPrefab;

        #endregion

        #region Public Methods

        public void SpawnEnemies(MovementBounds bounds, Player.Player player)
        {
            for (int i = 0; i < numberOfEnemies; i++)
            {
                SpawnEnemy(bounds, player);
            }
        }

        #endregion

        #region Private Methods

        private void SpawnEnemy(MovementBounds movementBounds, Player.Player player)
        {
            Vector2 enemyPosition = new Vector2(Random.Range(movementBounds.LeftBottom.x, movementBounds.RightTop.x), Random.Range(movementBounds.LeftBottom.y, movementBounds.RightTop.y));
            Enemy enemy = Instantiate(enemyPrefab, enemyPosition, Quaternion.identity, transform);
            enemy.Initialize(player);
        }

        #endregion
    }
}