using Inputs.Interfaces;
using UnityEngine;

namespace Enemies
{
    public class EnemyAI : MonoBehaviour, IDirectionProvider
    {
        #region Private Fields

        private Transform playerTransform;

        #endregion

        #region Public Methods

        public Vector2 GetDirection()
        {
            return (transform.position - playerTransform.position).normalized;
        }

        public void Initialize(Player.Player player)
        {
            playerTransform = player.transform;
        }

        #endregion
    }
}