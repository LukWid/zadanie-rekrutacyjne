using Inputs.Interfaces;
using Movement;
using UnityEngine;

namespace Enemies
{
    public class Enemy : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private MovementSettings movementSettings;
        [SerializeField]
        private float radius;
        [SerializeField]
        private LayerMask playerLayer;

        #endregion

        #region Private Fields

        private Player.Player player;

        private bool isPlayerNear;
        private bool isDead;
        private Mover mover;
        private IDirectionProvider directionProvider;
        private SpriteRenderer spriteRenderer;
        private Vector2 movementDirection;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            CollectReferences();
        }

        private void Start()
        {
            directionProvider.Initialize(player);
        }

        private void CollectReferences()
        {
            mover = GetComponent<Mover>();
            directionProvider = GetComponent<IDirectionProvider>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void FixedUpdate()
        {
            if (isDead)
            {
                return;
            }

            isPlayerNear = Physics2D.OverlapCircle(transform.position, radius, playerLayer);
        }

        private void Update()
        {
            if (isDead)
            {
                return;
            }

            if (isPlayerNear)
            {
                movementDirection = directionProvider.GetDirection();
                mover.Move(movementDirection, movementSettings.MovementSpeed);
            }
            else
            {
                mover.Move(Vector2.zero, 0);

            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, radius);
        }

        #endregion

        #region Public Methods

        public void Initialize(Player.Player player)
        {
            this.player = player;
        }

        public void Kill()
        {
            isDead = true;

            if (mover != null)
            {
                mover.Move(Vector2.zero, 0);
            }

            spriteRenderer.color = Color.gray;
        }

        #endregion
    }
}